const getCube = (x) => x ** 3;
let total = getCube(2);
console.log(`The cube of 2 is ${total}`);

const address = ["258", "Washington Ave NW", "California", "90011"]
const [houseNum, city, country, zipcode] = address;
console.log(`I live at ${houseNum} ${city}, ${country} ${zipcode}`);

class animal{
	animalDetails(type, weight, measurement){
		this.type = type;
		this.weight = weight;
		this.measurement = measurement;
	}
}
const croc = new animal()
croc.type = "saltwater crocodile";
croc.weight = "1075";
croc.measurement = "20 ft 3 in";
console.log(`Lolong was a ${croc.type}. He weighed at ${croc.weight} kgs ${croc.measurement}.`)

const numbers = [1, 2, 3, 4, 5]
numbers.forEach((number) => console.log(`${number}`));

class dogs{
	dogDetails(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const dog = new dogs();
dog.name = "Frankie";
dog.age = 5;
dog.breed = "Miniature Dachshund";
console.log(dog);